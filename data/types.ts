export interface MachineType {
  name: string;
  id: string;
}

export interface Thumbnail {
  thumbnail_path: string;
  alt: string;
  thumbnail_url: string;
  title: string;
}

export interface Image {
  image_url: string;
  image_mobile_url: string;
  order_number: number;
  alt: string;
  id: string;
  title: string;
  image_thumb_url: string;
}

export interface Detail {
  order_number: number;
  id: string;
  title: string;
  content: string;
}

export interface TechnicalInformation {
  order_number: number;
  name: string;
  details: Detail[];
  id: string;
}

export interface ProductStatus {
  name: string;
  id: string;
  status: number;
}

export interface MachineCategory {
  name: string;
  id: string;
}

export interface ApplicationType {
  name: string;
  id: string;
}

export interface Brand {
  name: string;
  id: string;
}

export interface Product {
  built_on: number;
  country: string;
  machine_types: MachineType[];
  control_unit_brand: string;
  thumbnail: Thumbnail;
  images: Image[];
  control_unit_brand_slug: string;
  technical_information: TechnicalInformation[];
  description: string;
  product_status: ProductStatus;
  availability: string;
  seo_title: string;
  path: string;
  machine_categories: MachineCategory[];
  model: string;
  production_hours?: number;
  id: string;
  sku: string;
  application_types: ApplicationType[];
  brand: Brand;
}

export interface Data {
  total_results: number;
  products: Product[];
}

export interface ProductsResponse {
  products: any;
  status: string;
  data: Data;
}
