import fs from "fs";
import { ProductsResponse } from "./types";

export function getAllProducts(): ProductsResponse {
  const products_data = fs.readFileSync("./data/response_example.json", "utf8");
  let products: ProductsResponse = JSON.parse(products_data);
  return products;
}
export function getProductsBySlug(slug?: string) {
  let products = getAllProducts();
  if (slug) {
    products.data.products = [
      products.data.products.find((p) => {
        return p.path.replace(/^\/product\//, "") == slug;
      }),
    ];
  }
  return products;
}
