import axios from "axios";
import type { GetStaticProps, NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { getAllProducts } from "../../data/repository";
import { Product, ProductsResponse } from "../../data/types";
import SearchBar from "../features/searchbar/SearchBar";

import styles from "../styles/Home.module.css";

interface ProductListProps {
  products: Product[];
}

function ProductList(props: ProductListProps) {
  return (
    <div className="bg-white">
      <SearchBar />
      <div className="max-w-2xl px-4 mx-auto sm:px-6 lg:max-w-7xl lg:px-8">
        <div className="grid grid-cols-1 mt-6 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
          {props.products.map((product) => (
            <div
              key={product.id}
              className="relative overflow-hidden border-4 cursor-pointer rounded-xl group"
            >
              <Link href={product.path}>
                <a>
                  <div className="w-full overflow-hidden bg-gray-200 rounded-t-md aspect-w-1 aspect-h-1 group-hover:opacity-75 lg:aspect-none ">
                    <img
                      src={product.images[0].image_mobile_url}
                      alt={product.images[0].alt}
                      className="object-cover object-center w-full h-full "
                      onError={({ currentTarget }) => {
                        currentTarget.onerror = null;
                        currentTarget.src = "/images/industrial.jpg";
                      }}
                    />
                  </div>
                  <div className="flex flex-col p-4">
                    <div className="mx-auto">
                      <p className="text-sm font-medium text-gray-900 ">
                        {product.model}
                      </p>
                    </div>
                    <div className="flex justify-between mt-4">
                      <p className="text-sm font-medium text-gray-700">
                        {product.country}
                      </p>
                      <p className="text-sm text-gray-700">
                        {product.built_on}
                      </p>
                      <p className="text-sm font-medium text-gray-700">
                        {product.control_unit_brand}
                      </p>
                    </div>
                  </div>
                </a>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
interface Props {
  products: Product[];
}

const IndexPage: NextPage<Props> = ({ products }) => {
  return (
    <div>
      <Head>
        <title>Gindumac - SELL AND BUY USED MACHINERY</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <div className="relative w-full h-48 md:h-72">
          <Image src="/images/industrial.jpg" layout="fill" objectFit="cover" />
          <h2 className="absolute text-2xl font-extrabold tracking-tight text-white transform -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2">
            Store
          </h2>
        </div>
      </header>
      <ProductList products={products} />
    </div>
  );
};

export default IndexPage;

export async function getStaticProps() {
  const resp = getAllProducts();
  const products = resp.data.products;
  return { props: { products } };
}
