import axios from "axios";
import Head from "next/head";
import { getAllProducts, getProductsBySlug } from "../../../../data/repository";
import { Product, ProductsResponse } from "../../../../data/types";
import DescriptionList from "../../../features/product/descriptionlist/DescriptionList";

interface ProductViewProps {
  product: Product;
}

function ProductView({ product }: ProductViewProps) {
  return (
    <div className="bg-white">
      <div className="max-w-2xl px-4 py-16 mx-auto sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 className="text-2xl font-extrabold tracking-tight text-gray-900"></h2>
        <div
          key={product.id}
          className="grid grid-cols-1 mt-6 gap-y-10 gap-x-6 lg:grid-cols-4 xl:gap-x-8"
        >
          <div className="overflow-hidden rounded-md min-h-80 aspect-w-1 aspect-h-1 hover:opacity-75 lg:h-80 lg:aspect-none lg:col-span-2">
            <img
              src={product?.images?.[0].image_mobile_url}
              alt={product?.images?.[0].alt}
              className="object-contain object-center w-full h-full lg:w-full lg:h-full"
            />
          </div>
          <div className="flex flex-col items-start justify-start lg:col-span-2">
            <DescriptionList product={product}/>
          </div>
        </div>
      </div>
    </div>
  );
}

interface Props {
  product: Product;
}

function ProductViewPage({ product }: Props) {
  return (
    <div>
      <Head>
        <title>Gindumac - SELL AND BUY USED MACHINERY</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <ProductView product={product} />
      </header>
    </div>
  );
}

export default ProductViewPage;

export async function getStaticProps({ params }) {
  const resp = getProductsBySlug(params.slug)
  const product = resp.data.products[0];
  return { props: { product } };
}

// function called at build time
export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const allProductsResponse: ProductsResponse = getAllProducts();
  // The list of paths that we want to pre-render
  const paths = allProductsResponse.data.products.map((product) => ({
    params: { slug: product.path.replace(/^\/product\//, "") },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false };
}
