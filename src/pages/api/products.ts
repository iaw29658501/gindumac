import type { NextApiHandler } from "next";
import { ProductsResponse } from "../../../data/types";
import { getProductsBySlug } from "../../../data/repository";
const countHandler: NextApiHandler = async (request, response) => {
  return response.status(200).json(getProductsBySlug(request.body.path));
};

export default countHandler;

