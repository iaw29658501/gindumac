import { useEffect, useState } from "react";
import { Product } from "../../../../data/types";

interface DescriptionListProps {
  product: Product;
}

export default function DescriptionList({ product }: DescriptionListProps) {
  const [description, setDescription] = useState({});
  useEffect(() => {
    setDescription({
      Model: product.model,
      Country: product.country,
      "Built On": product.built_on,
      Description: product.description,
    });
  }, [product]);
  return (
    <div className="overflow-hidden bg-white shadow sm:rounded-lg">
      <div className="px-4 py-5 sm:px-6">
        <h3 className="text-lg font-medium leading-6 text-gray-900">
          Product description
        </h3>
        <p className="max-w-2xl mt-1 text-sm text-gray-500">
          Personal details and application.
        </p>
      </div>
      <div className="border-t border-gray-200">
        <dl>
          {Object.keys(description).map((k) => (
            <div className="px-4 py-5 bg-gray-50 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6" key={`product-${k}`}>
              <dt className="text-sm font-medium text-gray-500">{k}</dt>
              <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {description[k]}
              </dd>
            </div>
          ))}
        </dl>
      </div>
    </div>
  );
}
