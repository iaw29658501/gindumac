import { render, screen } from "@testing-library/react";
import user from "@testing-library/user-event";
import { Provider } from "react-redux";
import DescriptionList from "./DescriptionList";

describe("<DescriptionList />", () => {
  it("renders the component", () => {
    render(
      <DescriptionList
        product={{
          built_on: 2017,
          country: "Poland",
          machine_types: [
            {
              name: "Machining centers (vertical)",
              id: "32dcdb32-1fdd-47c5-915a-e25135a9d456",
            },
          ],
          control_unit_brand: "FANUC",
          thumbnail: {
            thumbnail_path:
              "thumbnails/47/fanuc-robodrill-alpha-d21lib5_PL-MIL-FAN-2017-00001_1627483582.jpg",
            alt: "Front view of FANUC ROBODRILL Alpha-D21LiB5  machine",
            thumbnail_url:
              "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/thumbnails/47/fanuc-robodrill-alpha-d21lib5_PL-MIL-FAN-2017-00001_1627483582.jpg",
            title: "FANUC ROBODRILL Alpha-D21LiB5  Front view",
          },
          images: [
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/1d47/front-view-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/1d47/front-view-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 1,
              alt: "Front view of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "2fcae771-ac96-499e-9d12-182ce8f61d47",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Front view",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/1d47/front-view-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/40fe/left-side-view-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/40fe/left-side-view-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 2,
              alt: "Left side view of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "a6b0f892-f623-439c-9d5e-0ca4c2fc40fe",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Left side view",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/40fe/left-side-view-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/1bb0/right-side-view-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/1bb0/right-side-view-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 4,
              alt: "Right side view of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "bf915784-625f-4406-abd9-277c48d41bb0",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Right side view",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/1bb0/right-side-view-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/3768/back-view-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/3768/back-view-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 6,
              alt: "Back view of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "3e0fd49d-bcc5-4196-899e-3dbe8d373768",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Back view",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/3768/back-view-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/26a7/working-room-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/26a7/working-room-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 8,
              alt: "Working room of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "aa509300-e866-4931-82d2-39b4510726a7",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Working room",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/26a7/working-room-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/62e2/working-room-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/62e2/working-room-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 8,
              alt: "Working room of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "cbf3eff0-bf34-4c01-a0ed-5b99429562e2",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Working room",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/62e2/working-room-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/e9f2/control-unit-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/e9f2/control-unit-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 9,
              alt: "Control unit of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "4a6580e5-8a8b-4c8c-bb7b-d927133de9f2",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Control unit",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/e9f2/control-unit-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/6d3e/nameplate-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/6d3e/nameplate-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 11,
              alt: "Nameplate of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "46940fbc-969c-48c5-87a5-2a67ca786d3e",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Nameplate",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/6d3e/nameplate-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
            {
              image_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery/4bad/accessories-of-fanuc-robodrill-alpha-d21lib5-machine-wm.jpg",
              image_mobile_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-mobile/4bad/accessories-of-fanuc-robodrill-alpha-d21lib5-machine-mobile-wm.jpg",
              order_number: 12,
              alt: "Accessories of FANUC ROBODRILL Alpha-D21LiB5  machine",
              id: "96f8e5a7-fbc2-4f7c-9d12-73f514e64bad",
              title: "FANUC ROBODRILL Alpha-D21LiB5  Accessories",
              image_thumb_url:
                "https://gindumac-p2p.s3.eu-central-1.amazonaws.com/gallery-thumb/4bad/accessories-of-fanuc-robodrill-alpha-d21lib5-machine-thumb-wm.jpg",
            },
          ],
          control_unit_brand_slug: "fanuc",
          technical_information: [
            {
              order_number: 0,
              name: "General Data",
              details: [
                {
                  order_number: 0,
                  id: "8333f7ee-4de3-4e37-b9d4-86f0ab266e08",
                  title: "Measurements width",
                  content: "2165 mm",
                },
                {
                  order_number: 0,
                  id: "90830c8b-c446-4ebc-8520-56f03ef4ea7d",
                  title: "Measurements depth",
                  content: "1900 mm",
                },
                {
                  order_number: 0,
                  id: "c30c8bf8-7081-46c4-92be-135620615631",
                  title: "Measurements height",
                  content: "2082 mm",
                },
                {
                  order_number: 0,
                  id: "639b7888-4947-4653-9313-03e7b68f3e2d",
                  title: "Machine weight",
                  content: "2300 KG",
                },
              ],
              id: "e52dbb60-6f2c-47a8-9c92-54d6c3ad2ab9",
            },
            {
              order_number: 1,
              name: "Control Unit",
              details: [
                {
                  order_number: 0,
                  id: "8cf07896-911b-471e-90cc-86a65950297d",
                  title: "Brand",
                  content: "FANUC",
                },
                {
                  order_number: 0,
                  id: "d1803fa6-f213-4e52-9cbc-66892ac34560",
                  title: "Model",
                  content: "31i-MODEL B5",
                },
              ],
              id: "2d9ad5b9-d0a1-45d3-8eda-9f1deac7f672",
            },
            {
              order_number: 2,
              name: "Main Drive",
              details: [
                {
                  order_number: 0,
                  id: "01689030-8ce6-4b5d-865a-57356932fa2d",
                  title: "Spindle speed range",
                  content: "10000",
                },
                {
                  order_number: 0,
                  id: "07b82550-9eb8-4073-9915-e13cc124a680",
                  title: "Tool taper",
                  content: "BBT30",
                },
                {
                  order_number: 0,
                  id: "0f8a205b-1a9f-4985-a320-89a072cc9fac",
                  title: "Number of axis",
                  content: "3",
                },
              ],
              id: "e85fb60f-637e-4283-9c96-448eda8842b6",
            },
            {
              order_number: 3,
              name: "Movement",
              details: [
                {
                  order_number: 0,
                  id: "65fc2244-5db7-4048-8a68-62eb413703a8",
                  title: "X-Axis Movement",
                  content: "700 mm",
                },
                {
                  order_number: 0,
                  id: "aa521770-3f9c-447d-a296-e13eefd44048",
                  title: "Y-Axis Movement",
                  content: "400 mm",
                },
                {
                  order_number: 0,
                  id: "7ee6f4ce-e23a-46f6-8f50-a0d3dedabf4a",
                  title: "Z-Axis Movement",
                  content: "330 mm",
                },
              ],
              id: "cf9b4472-f810-4db8-a025-4674ece878b3",
            },
            {
              order_number: 4,
              name: "Table",
              details: [
                {
                  order_number: 0,
                  id: "a1182d9f-92a3-480f-a564-e671e07a0f15",
                  title: "Outer length",
                  content: "850 mm",
                },
                {
                  order_number: 0,
                  id: "564f2e3b-c123-481b-9daf-5b4adc2b1ed6",
                  title: "Outer width",
                  content: "410 mm",
                },
                {
                  order_number: 0,
                  id: "7301353a-0d29-43a2-ba8f-593f9339aea7",
                  title: "Table load",
                  content: "300 KG",
                },
              ],
              id: "c68e206a-5177-4520-a88b-2bb850e39a34",
            },
            {
              order_number: 5,
              name: "Tools",
              details: [
                {
                  order_number: 0,
                  id: "ed05b02a-ae50-4105-adee-b7b185e76b67",
                  title: "Max weight",
                  content: "3 KG",
                },
                {
                  order_number: 0,
                  id: "43207569-a694-4153-9521-d46d869f150c",
                  title: "Max tool diameter",
                  content: "80 mm",
                },
                {
                  order_number: 0,
                  id: "3521b134-6b67-4822-9213-10bb9f17a6b4",
                  title: "Max tool length",
                  content: "250 mm",
                },
              ],
              id: "49028650-f0a4-4a70-b545-eae5a8f9e65e",
            },
            {
              order_number: 6,
              name: "Media",
              details: [
                {
                  order_number: 0,
                  id: "2fed4fdb-2a7f-4e5e-b68e-1cd101ffe47a",
                  title: "Main electronic connection",
                  content: "220 V / 50-60 Hz",
                },
              ],
              id: "d79b55a3-a812-4c93-a86f-a62b71315603",
            },
          ],
          description:
            "This FANUC ROBODRILL Alpha-D21LiB5 Vertical Machining Center was manufactured in the year 2017 in Japan. This 3 axis machine has been working for 2232 hours. It operated through a FANUC 31i-MODEL B5 control unit. It incorporates a spindle speed up to 10000 rpm and a table size of 850 x 410 mm. It includes some tooling with holders, 1 vice and full documentation available. Machine in very good condition ready for tests.",
          product_status: {
            name: "published",
            id: "0ab0feec-0f89-4d5d-aa86-c9953f0bebc4",
            status: 1,
          },
          availability: "IMMEDIATELY",
          seo_title: "FANUC ROBODRILL Alpha-D21LiB5 Vertical Machining Center",
          path: "/product/fanuc-robodrill-alpha-d21lib5_PL-MIL-FAN-2017-00001",
          machine_categories: [
            {
              name: "Machine Tools",
              id: "a0d9d006-465c-4044-9d24-ef0eb4470121",
            },
          ],
          model: "ROBODRILL Alpha-D21LiB5",
          production_hours: 2232,
          id: "2d8c04d3-41d9-4e13-98a4-2b4f835a7473",
          sku: "PL-MIL-FAN-2017-00001",
          application_types: [
            {
              name: "Milling",
              id: "62c734b2-692a-4c5c-aeec-27357e28a846",
            },
          ],
          brand: {
            name: "FANUC",
            id: "2fa14980-7551-4c78-a181-42325d7212ee",
          },
        }}
      />
    );

    expect(screen.getByText("ROBODRILL Alpha-D21LiB5")).toBeInTheDocument();
    expect(screen.getByText("Poland")).toBeInTheDocument();
    expect(screen.getByText("2017")).toBeInTheDocument();
    expect(screen.getByText("This FANUC ROBODRILL Alpha-D21LiB5 Vertical Machining Center was manufactured in the year 2017 in Japan. This 3 axis machine has been working for 2232 hours. It operated through a FANUC 31i-MODEL B5 control unit. It incorporates a spindle speed up to 10000 rpm and a table size of 850 x 410 mm. It includes some tooling with holders, 1 vice and full documentation available. Machine in very good condition ready for tests.")).toBeInTheDocument();
  });
});
