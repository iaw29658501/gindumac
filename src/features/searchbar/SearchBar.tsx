import { useState } from "react";

import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { BsSearch } from "react-icons/bs";
function SearchBar() {
  return (
    <div className="relative max-w-3xl pt-2 mx-auto text-gray-600">
      <input
        className="w-full h-10 px-5 pr-16 text-sm bg-white border-2 border-gray-300 rounded-lg focus:outline-none"
        type="search"
        name="search"
        placeholder="Search by country or model"
      />
      <button type="submit" className="absolute top-0 right-0 mt-5 mr-4">
        <BsSearch className="w-4 h-4 text-gray-600 fill-current" />
      </button>
    </div>
  );
}

export default SearchBar;
